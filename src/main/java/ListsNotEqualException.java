public class ListsNotEqualException extends Exception {
    private String detailMessage = "ERROR: LISTS ARE NOT EQUAL";

    public String getMessage() {
        return detailMessage;
    }

    public ListsNotEqualException() {
        super();
    }

    public ListsNotEqualException(String message) {
        super(message);
    }

    public ListsNotEqualException(String message, Throwable cause) {
        super(message, cause);
    }

    public ListsNotEqualException(Throwable cause) {
        super(cause);
    }
}
