import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.sort;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;

public class ListUtils {

    private static List sorter(final List list) {
        if(list.isEmpty()){
            return Collections.EMPTY_LIST;
        } else if (list.get(0) instanceof Integer) {
            sort(list);
        } else if(list.get(0) instanceof String) {
            sort(list);
        } else {
            sort(list, nullsFirst(naturalOrder()).thenComparing(naturalOrder()));
        }
        return list;
    }

    static List sortMultiTypedList(final List list){
        List<Object> listOfObj = new ArrayList<>();
        List<Integer> listOfInts = new ArrayList<>();
        List<String> listOfStrings = new ArrayList<>();

        list.forEach(value -> {
            if (value instanceof Integer) {
                listOfInts.add(Integer.valueOf(value.toString()));
            } else if (value instanceof String) {
                listOfStrings.add(Objects.toString(value));
            } else {
                listOfObj.add(value);
            }
        });

        return List.of(
                sorter(listOfObj),
                sorter(listOfInts),
                sorter(listOfStrings));
    }
}
