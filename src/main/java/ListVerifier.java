import java.util.*;

import org.apache.commons.collections4.CollectionUtils;

class ListVerifier {

    static boolean ifEquallySized(List<?> list, List<?> list2) {
        if(Objects.nonNull(list) && Objects.nonNull(list2))
            return list.size() == list2.size();
        else
            return false;
    }

    static boolean compareMultiTypedLists(List<?> list, List<?> list2) {
        return ifEquallySized(list, list2) && ListUtils.sortMultiTypedList(list).equals(ListUtils.sortMultiTypedList(list2));
    }

    boolean verifyUsingHashSets(final List<?> list1, List<?> list2) {
        return (ifEquallySized(list1, list2)) && new HashSet(list1).equals(new HashSet(list2));
    }

    boolean verifyUsingCollectionUtils(final List<?> list, List<?> list2) {
        return ifEquallySized(list, list2) && CollectionUtils.isEqualCollection(list, list2);
    }

    boolean verifyUsingStreamer(final List<?> list, final List<?> list2) {
        return ifEquallySized(list, list2)
                && list.stream().takeWhile(Objects::nonNull).allMatch(list2::contains)
                && list2.stream().takeWhile(Objects::nonNull).allMatch(list::contains);
    }

    boolean verifyUsingSortedLists(final List<?> list, final List<?> list2) {
        return compareMultiTypedLists(list, list2);
    }

    void equalityError(boolean areListEquals) throws ListsNotEqualException {
        if (!areListEquals) {
            throw new ListsNotEqualException();
        }
    }
}
