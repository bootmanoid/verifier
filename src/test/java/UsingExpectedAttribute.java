import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;

@RunWith(value = Parameterized.class)
public class UsingExpectedAttribute extends ExceptionTestData {
    private static final Logger LOG = LoggerFactory.getLogger(UsingExpectedAttribute.class);
    private static final ListVerifier LIST_VERIFIER = new ListVerifier();

    private List<?> list1;
    private List<?> list2;

    public UsingExpectedAttribute(List<?> list1, List<?> list2) {
        this.list1 = list1;
        this.list2 = list2;

        LOG.info(String.valueOf(list1) + " vs " + String.valueOf(list2));
    }

    @Parameters
    public static Collection<Object[]> parameters(){
        return data();
    }

    @Test(expected = ListsNotEqualException.class)
    public void verifyIfEqual_HashSets() throws ListsNotEqualException {
        boolean areListEqual = LIST_VERIFIER.verifyUsingHashSets(list1, list2);
        LIST_VERIFIER.equalityError(areListEqual);
    }


    @Test(expected = ListsNotEqualException.class)
    public void verifyIfEqual_CollectionUtils() throws ListsNotEqualException {
        boolean areListEqual = LIST_VERIFIER.verifyUsingCollectionUtils(list1, list2);
        LIST_VERIFIER.equalityError(areListEqual);

    }

    @Test(expected = ListsNotEqualException.class)
    public void verifyIfEqual_Streamer() throws ListsNotEqualException {
        boolean areListEqual = LIST_VERIFIER.verifyUsingStreamer(list1, list2);
        LIST_VERIFIER.equalityError(areListEqual);
    }

    @Test(expected = ListsNotEqualException.class)
    public void verifyIfEqual_SortedLists() throws ListsNotEqualException {
        boolean areListEqual = LIST_VERIFIER.verifyUsingSortedLists(list1, list2);
        LIST_VERIFIER.equalityError(areListEqual);
    }
}
