import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.junit.runners.Parameterized.*;

@RunWith(value = Parameterized.class)
public class TestClass {
    private static final Logger LOG = LoggerFactory.getLogger(ListVerifier.class);
    private static final ListVerifier LIST_VERIFIER = new ListVerifier();

    private List<?> list1;
    private List<?> list2;
    private boolean isEqual;

    public TestClass(List<?> list1, List<?> list2, boolean isEqual) {
        this.list1 = list1;
        this.list2 = list2;
        this.isEqual = isEqual;

        LOG.info(String.valueOf(list1) + " vs " + String.valueOf(list2));
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {
                        Arrays.asList("X", 2, 9, "Y"), Arrays.asList("X", 2, 9), false
                },
                {
                        Arrays.asList(0, 2, 9, null), Arrays.asList(0, 2, 9, null), true
                },
                {
                        null, Arrays.asList(0, 2, 3), false
                },
                {
                        Arrays.asList("TEST", "DATA", 0, 0), Arrays.asList(0, "TEST", 0, "DATA"), true
                },
                {
                        Arrays.asList(5, null), Arrays.asList(5, 5, null), false
                },
                {
                        new LinkedList<>(Arrays.asList(5, 0)), Arrays.asList(5, 0), true
                },
                {
                        new LinkedList<>(Arrays.asList(0, 0, 0, 0, 5)), Arrays.asList(5, 0, 5, 4, 5), false
                }
        });
    }

    @Test
    public void verifyIfEqual_HashSets() {
        assertThat(LIST_VERIFIER.verifyUsingHashSets(list1, list2), is(isEqual));
    }


    @Test
    public void verifyIfEqual_CollectionUtils() {
        assertThat(LIST_VERIFIER.verifyUsingCollectionUtils(list1, list2), is(isEqual));
    }

    @Test
    public void verifyIfEqual_Streamer() {
        assertThat(LIST_VERIFIER.verifyUsingStreamer(list1, list2), is(isEqual));
    }

    @Test
    public void verifyIfEqual_SortedLists() {
        assertThat(LIST_VERIFIER.verifyUsingSortedLists(list1, list2), is(isEqual));
    }
}
