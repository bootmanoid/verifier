import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


import static org.junit.Assert.fail;

@RunWith(value = Parameterized.class)
public class UsingTryCatch extends ExceptionTestData {
    private static final Logger LOG = LoggerFactory.getLogger(UsingTryCatch.class);
    private static final ListVerifier LIST_VERIFIER = new ListVerifier();

    private static final String ASSERT_MSSG = "Expected an IndexOutOfBoundsException to be thrown";
    private static final String ERROR_MSSG = "ERROR: LISTS ARE NOT EQUAL";

    private List<?> list1;
    private List<?> list2;

    public UsingTryCatch(List<?> list1, List<?> list2) {
        this.list1 = list1;
        this.list2 = list2;

        LOG.info(String.valueOf(list1) + " vs " + String.valueOf(list2));
    }

    @Parameters
    public static Collection<Object[]> parameters(){
        return data();
    }

    @Test
    public void verifyIfEqual_HashSets() {
        boolean areListEqual = LIST_VERIFIER.verifyUsingHashSets(list1, list2);

        try {
            LIST_VERIFIER.equalityError(areListEqual);
            fail(ASSERT_MSSG);
        } catch (ListsNotEqualException exception) {
            LOG.error(ERROR_MSSG);
            assertThat(exception.getMessage(), is(ERROR_MSSG));
        }
    }

    @Test
    public void verifyIfEqual_CollectionUtils() {
        boolean areListEqual = LIST_VERIFIER.verifyUsingCollectionUtils(list1, list2);

        try {
            LIST_VERIFIER.equalityError(areListEqual);
            fail(ASSERT_MSSG);
        } catch (ListsNotEqualException exception) {
            LOG.error(ERROR_MSSG);
            assertThat(exception.getMessage(), is(ERROR_MSSG));
        }

    }

    @Test
    public void verifyIfEqual_Streamer() {
        boolean areListEqual = LIST_VERIFIER.verifyUsingStreamer(list1, list2);

        try {
            LIST_VERIFIER.equalityError(areListEqual);
            fail(ASSERT_MSSG);
        } catch (ListsNotEqualException exception) {
            LOG.error(ERROR_MSSG);
            assertThat(exception.getMessage(), is(ERROR_MSSG));
        }
    }

    @Test
    public void verifyIfEqual_SortedLists() {
        boolean areListEqual = LIST_VERIFIER.verifyUsingSortedLists(list1, list2);

        try {
            LIST_VERIFIER.equalityError(areListEqual);
            fail(ASSERT_MSSG);
        } catch (ListsNotEqualException exception) {
            LOG.error(ERROR_MSSG);
            assertThat(exception.getMessage(), is(ERROR_MSSG));
        }
    }
}
