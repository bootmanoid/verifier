import java.util.*;

class ExceptionTestData {

    ExceptionTestData() {
    }


    static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {
                        Arrays.asList("X", 2, 9, "Y"), Arrays.asList("X", 2, 9)
                },
                {
                        Arrays.asList(null, null), Arrays.asList(0, 2, 9, null)
                },
                {
                        null, Arrays.asList(0, 2, 3)
                },
                {
                        null, null
                },
                {
                        new LinkedList<>(Arrays.asList(0, 0, 0, 0, 5)), Arrays.asList(5, 0, 5, 4, 5)
                },
                {
                        Arrays.asList("X", "Y", "Z"), Arrays.asList("X", "", "Z")
                },
                {
                        Arrays.asList(""), null
                }
        });
    }
}
