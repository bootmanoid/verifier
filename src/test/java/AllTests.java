import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        TestClass.class,
        UsingTryCatch.class,
        UsingExpectedAttribute.class,
        UsingExpectedException.class
})

public class AllTests {

}